const User = require('../../Model/User');

/**
 * i have found that the function above is repeated 
 * in each passport auth middleware
 * so, i extracted here
 */

module.exports = (accessToken, refreshToken, profile, cb) => {
  User
    .findOrCreate({
      where: { id: profile.id }, 
      defaults: {
        id: profile.id,
        displayName: profile.displayName,
        email: profile.emails && profile.emails.length && profile.emails[0].value ? profile.emails[0].value : '',
        provider: profile.provider,
      }
    })
    .then(([user, created]) => {
      return cb(null, user);
    }).catch(err => {
      return cb(err, null);
    }); 
}
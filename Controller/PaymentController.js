const debug = require('debug')('app:PaymentController');
const Payment = require('../Model/Payment');
const User = require('../Model/User');

module.exports = {

  async payment(req, res) {

    debug(req.body.details);
    const payment = req.body.details;

    /**
     * verify payment
     */
    if(payment.status.toLowerCase() === 'completed' &&
    payment.purchase_units[0].amount.currency_code.toLowerCase() === 'usd' &&
    payment.purchase_units[0].amount.value.toLowerCase() === '14.99') {
    
      /**
       * prepare the data to be inserted into database
       */
      let payer = '';
      try {
        payer = payment.payer;
      } catch (error) {}

      let create_time = '';
      try {
        create_time = payment.create_time;
      } catch (error) {}

      let payee = '';
      try {
        payee = payment.purchase_units[0].payee;
      } catch (error) {}

      let amount = '';
      try {
        amount = payment.purchase_units[0].amount.value;
      } catch (error) {}

      let currency_code = '';
      try {
        currency_code = payment.purchase_units[0].amount.currency_code;
      } catch (error) {}

      try {

        /**
         * update user subscription
         */
        await User.update({
            subscription: create_time
          },{ 
            where: { id: JSON.parse(req.cookies.auth).id },
        });

        /**
         * insert payment details into database
         */
        await Payment.create({
          user_id: JSON.parse(req.cookies.auth).id,
          payer,
          create_time,
          payee,
          amount,
          currency_code
        });
  
        res.status(201).end('Paid successfully!');
      } catch (error) {
        throw error;
      }

    } else {
      return res.status(400).end('Your payment is not normal, please contact us');
    }
    
  }

}
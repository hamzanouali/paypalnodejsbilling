
  var billingPlan = req.body.id;
  var billingAgreementAttributes;
  var isoDate = new Date();

  isoDate.setSeconds(isoDate.getSeconds() + 4);
  isoDate.toISOString().slice(0, 19) + 'Z';

  billingAgreementAttributes = {
    name: 'Standard Membership',
    description: 'Food of the World Club Standard Membership',
    start_date: isoDate,
    plan: {
      id: billingPlan
    },
    payer: {
      payment_method: 'paypal'
    }
  };

  var links = {};

  // Use activated billing plan to create agreement
  paypal.billingAgreement.create(billingAgreementAttributes, function (error, billingAgreement){
    if (error){
      console.error(JSON.stringify(error));
      throw error;
    } else {
      // Capture HATEOAS links
      billingAgreement.links.forEach(function(linkObj){
        links[linkObj.rel] = {
          href: linkObj.href,
          method: linkObj.method
        };
      })

      console.log(billingAgreement.links);
      // If redirect url present, redirect user
      if (links.hasOwnProperty('approval_url')){
        //REDIRECT USER TO links['approval_url'].href
      } else {
        console.error('no redirect URI present');
      }
    }
  });
const request = require('supertest');
const faker = require('faker')
const User= require('../../Model/UserModel')
const jwt = require('jsonwebtoken')
const config = require('config')
const bcrypt = require('bcrypt')
const _ = require('lodash')


let server

describe ('RegisterController', () => {

  beforeEach(() => {
    server = require('../../index')
  })

  afterEach( async () => {
    await server.close()
  })

  describe ('/user/register', () => {
    it ('Should register successfully', async () => {

      // password 
      const password = faker.internet.password()
      let user = {
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: password,
        passwordConfirmation: password
      } 

      const result = await request(server)
        .post('/user/register')
        .send(user)

      expect(result.status).toBe(201)
      expect(_.pick(user, ['name', 'email'])).toMatchObject(_.pick(result, ['name', 'email']))
    })

    it ('Wrong EMAIL: should not pass validation', async () => {
      
      const user = { 
        name: faker.name.findName(), 
        email: 'heja', 
        password: 'hola', 
        passwordConfirmation: 'hola' 
      }

      const result = await request(server)
        .post('/user/register')
        .send(user)

      expect(result.status).toBe(400)
      expect(result.body).not.toMatchObject(_.pick(user, ['name', 'email']))

    })

    it ('Wrong PASSWORD CONFIRMATION: should not pass validation', async () => {
      
      const user = { 
        name: faker.name.findName(), 
        email: faker.internet.email(), 
        password: 'hola', 
        passwordConfirmation: 'hola1' 
      }

      const result = await request(server)
        .post('/user/register')
        .send(user)

      expect(result.status).toBe(400)
      expect(result.body).not.toMatchObject(_.pick(user, ['name', 'email']))

    })

    it ('User EXISTS : should not sign up', async () => {
      // getting user
      let user = await User.findOne()

      user = _.pick(user, ['name','email', 'password'])
      user.passwordConfirmation = user.password

      const result = await request(server)
        .post('/user/register')
        .send(user)

      expect(result.status).toBe(403)

    })

  })

  describe ('/user/login', () => {

    it ('Should login successfully', async () => {
      
      let password_example = 'password'

      const user = new User({
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: password_example
      })

      await user.save()

      const result = await request(server)
      .post('/user/login')
      .send({ 
        email: user.email, 
        password: password_example 
      })

      expect(result.status).toBe(200)
      const jwt_token = jwt.verify(result.body.jwt, config.get('jwtPrivateKey'))  
      expect(_.pick(jwt_token.user, ['name', 'email'])).toMatchObject(_.pick(user, ['name', 'email']))

    })

    it ('Wrong EMAIL: should not pass validation', async () => {

      let user = {
        email : 'wrong email',
        password: '1256'
      }


      const result = await request(server)
        .post('/user/login')
        .send(user)

      expect(result.status).toBe(400)

    })

    it ('Real EMAIL and wrong PASSWORD: should get 401', async () => {

      // getting user
      let user = await User.findOne()

      user = { 
        email: user.email,
        password: faker.internet.password()
      }

      const result = await request(server)
        .post('/user/login')
        .send(user)

      expect(result.status).toBe(401)

    })

    it ('Wrong CREDENTIALS : should get 401', async () => {
      
      let user = {
        email : faker.internet.email(),
        password: faker.internet.password()
      }


      const result = await request(server)
        .post('/user/login')
        .send(user)

      expect(result.status).toBe(401)

    })

  })

})
module.exports = {

  controller : 'controller_name',
  prefix: 'prefix_should_be_here',

  get : [
    // All GET routes are listed here
  ],

  post: [
     // All POST routes are listed here
  ],

  put: [
    // All PUT routes are listed here
  ],

  delete: [
    // All DELETE routes are listed here
  ]

}
/**
 * Runs all the tests related to Model commands
 */
module.exports = () => {

const { exec } = require('child_process');
const { execSync } = require('child_process');
const fs = require('fs')
const log = require('../../../Core/CLI/log')

// the name of Model to be created for testing
let Model = 'TestingModel'

let directory = './Model/'



describe('Model Commands', () => { 

  describe ('Create Commands: ', () => {

    afterEach((done) => {
      // delete the created file
      fs.unlink(`${directory}${Model}.js`, (err) => {
        // error case
        if(err) throw err

        done()

      })
    })

    it ('cli create-model TestingModel', (done) => {

      // execute commande
      exec(`cli create-model ${Model}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Model}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })

    })

    it ('cli cm TestingModel', (done) => {

      // execute commande
      exec(`cli cm ${Model}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Model}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

    it ('cli create-model Folder/TestingModel', (done) => {

      // updating Model
      Model = 'Folder/TestingModel'

      // execute commande
      exec(`cli create-model ${Model}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Model}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

    it ('cli create-model /Folder/TestingModel', (done) => {

      // updating Model
      Model = '/Folder/TestingModel'

      // execute commande
      exec(`cli create-model ${Model}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Model}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

    it ('cli cm Folder/TestingModel', (done) => {

      // updating Model
      Model = 'Folder/TestingModel'

      // execute commande
      exec(`cli cm ${Model}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Model}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

  })


  describe ('Delete Commands: ', () => {

    beforeEach (() => {

      /* 
      before each test, create a new directory with a new file
      in order to test deleting these files 
      */

      execSync(`cli cm ${Model}`)
      execSync(`cli cm Folder/${Model}`)

    })

    it ('cli delete-model TestingModel', (done) => {

      // execute commande
      exec(`cli delete-model ${Model}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Model}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

    it ('cli delete-model Folder/TestingModel', (done) => {

      // updating Model
      Model = 'Folder/TestingModel'

      // execute commande
      exec(`cli delete-model ${Model}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Model}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

    it ('cli dm /Folder/TestingModel', (done) => {

      // updating Model
      Model = '/Folder/TestingModel'

      // execute commande
      exec(`cli dm ${Model}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Model}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

    it ('cli dm TestingModel', (done) => {

      // execute commande
      exec(`cli dm ${Model}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Model}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

  })

})

}
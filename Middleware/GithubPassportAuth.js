const {app, express} = require('../initExpress');
const { passport, PassportMiddlewareCallback } = require('./passport/initPassport');
const Strategy = require('passport-github').Strategy;

passport.use(new Strategy({
    clientID: '97c4601a5d9102580e20',
    clientSecret: '8c45b2dff5e0b3b853a6de17bb5c7e33ce50f5cf',
    callbackURL: "http://localhost:7000/auth/github/callback",
    profileFields: ['id', 'emails', 'displayName']
  },
  (accessToken, refreshToken, profile, cb) => PassportMiddlewareCallback(accessToken, refreshToken, profile, cb)
));

module.exports = (params = false) => {
  return (req, res, next) => {
    passport.authenticate('github', { scope: ['email'], redirectFailure: '/login' })(req, res, next);
  }
}
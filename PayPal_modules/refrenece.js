app.get('/success', (req, res) => {
  var token = req.query.token;

  paypal.billingAgreement.execute(token, {}, function (error, billingAgreement){
    if (error){
      console.error(JSON.stringify(error));
      throw error;
    } else {
      res.json(JSON.stringify(billingAgreement));
      console.log('Billing Agreement Created Successfully');
    }
  });
});

app.post('/buy', (req, res) => {

  // Build PayPal payment request
  const payReq = {
    intent:'sale',
    payer:{
      payment_method:'paypal'
    },
    redirect_urls:{
      return_url:'http://localhost:7000/process',
      cancel_url:'http://localhost:7000/cancel'
    },
    transactions:[{
      amount:{
        total:'25.00',
        currency:'USD'
      },
      description:'This is the payment transaction description.'
    }]
  };

  /**
   * Create a Payment
   */
  paypal.payment.create(payReq, function(error, payment){

    if(error){
      console.error(JSON.stringify(error));
    } else {

      // Capture HATEOAS links
      payment.links.forEach(function(link){
        // If the redirect URL is present, redirect the customer to that URL
        if (link.rel === 'approval_url'){
          // Redirect the customer to links['approval_url'].href
          res.redirect(link.href);
        } 
      });

    }
  });

});

app.get('/process', (req, res) => {

  var paymentId = req.query.paymentId;
  var payerId = { payer_id: req.query.PayerID };

  paypal.payment.execute(paymentId, payerId, function(error, payment){
    if(error){
      console.error(JSON.stringify(error));
    } else {
      if (payment.state == 'approved'){
        console.log('payment completed successfully');
      } else {
        console.log('payment not successful');
      }
    }
  });

});
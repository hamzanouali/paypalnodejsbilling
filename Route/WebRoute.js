module.exports = {

  controller : 'WebController',
  prefix: '',

  get : [
    // All GET routes are listed here

    ['/create', 'app']
  ],

  post: [
     // All POST routes are listed here
  ],

  put: [
    // All PUT routes are listed here
  ],

  delete: [
    // All DELETE routes are listed here
  ]

}
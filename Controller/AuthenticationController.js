module.exports = {

  async render(req, res) {
    res.render('login');
  },

  async logout(req, res) {
    req.logout();
    res.cookie('auth', {});
    res.redirect('/login');
  },

  async login(req, res) {

    if(req.user.id) {
      res.cookie('auth', JSON.stringify(req.user), { maxAge: 60 * 60 * 60 * 24 });
      res.redirect('/home');
    } else {
      res.status(401).render('401');
    }

  },

  async renderHome(req, res) {
    res.render('home', { user: req.cookies.auth });
  },

  async subscribe(req, res) {
    res.end('Subscribe is here...');
  }

}
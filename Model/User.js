const {Sequelize, sequelize} = require('./sequelize');

module.exports = sequelize.define('User', {
    // attributes
    id: {
      type: Sequelize.STRING,
      allowNull: false,
      primaryKey: true
    },
    displayName: {
      type: Sequelize.STRING,
      allowNull: false
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false
    }, 
    provider: {
      type: Sequelize.STRING,
      allowNull: false
    },
    subscription: {
      //type: Sequelize.ENUM('paid', 'test'),
      type: Sequelize.STRING,
      defaultValue: 'test',
      allowNull: false
    }
  });

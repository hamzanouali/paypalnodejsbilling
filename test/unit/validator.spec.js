const validator = require('../../Core/validator')
const Joi = require('joi')

describe ('Testing validator (Core/validator.js) methods.', () => {

  describe ('validate method: for validating inputs.', () => {

    it ('validate should pass successfully', () => {

      /**
       * validate method and how it works ?
       * if the validation fails, will return an error and a 400 response with 
       * error details. otherways won't return any thing (void) 
       */

      // object that contains some inputs
      const payload = {
        name: 'Hamza Nouali'
      }

      // mocking res
      // res in real examples should carry express resonse
      let res = {
        statusResult : 200,
        error: null,
        // in order to assert that this method got a call in real example
        status (number) {
          this.statusResult = number
          return this
        },
        // in order to assert that this method got a call in real example
        json (object) {
          this.error = object
        }
      }

      // pass that object to validate method and asign the result to a constant
      const result = validator.validate(payload, {
        name: Joi.string().required()
      }, res)

      // checking the value of that constant
      // it should't contain anything, that means it does not return any error
      expect(result).not.toBeTruthy()
      
      // asserting that res got no call
      expect(res.statusResult).toBe(200)
      expect(res.error).toBe(null)

    })

    it ('validate should not pass (should fail).', () => {

      /**
       * validate method and how it works ?
       * if the validation fails, will return an error and a 400 response with 
       * error details. otherways won't return any thing (void) 
       */

      // object that contains some inputs
      const payload = {
        // it should be a string,but it's intentional
        name: 2
      }

      // mocking res
      // res in real examples should carry express resonse
      let res = {
        statusResult : 200,
        error: null,
        // in order to assert that this method got a call in real example
        status (number) {
          this.statusResult = number
          return this
        },
        // in order to assert that this method got a call in real example
        json (object) {
          this.error = object
        }
      }

      // pass that object to validate method and asign the result to a constant
      const result = validator.validate(payload, {
        name: Joi.string().required()
      }, res)

      // checking the value of that constant
      // it should contain an error
      expect(result.message).toBeDefined()
      
      // asserting that res got a call
      expect(res.statusResult).toBe(400)
      expect(res.error).not.toBe(null)

    })

  })

})
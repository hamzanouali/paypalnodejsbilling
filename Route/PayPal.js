module.exports = {

  controller : 'PayPalController',
  prefix: '',

  get : [
    // All GET routes are listed here
    ['/admin/paypal/create-a-plan', 'createPlan'],
    ['/admin/paypal/list-plans', 'list'],
    ['/user/subscribe', 'createBillingAgreement'],
    ['/user/paypal/billing/success', 'executeBilling'],
  ],

  post: [
     // All POST routes are listed here
    ['/admin/paypal/create-a-plan', 'create'],
    ['/user/paypal/create-a-billing-agreement', 'createBilling'],
  ],

  put: [
    // All PUT routes are listed here
  ],

  delete: [
    // All DELETE routes are listed here
  ]

}
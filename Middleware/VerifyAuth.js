const debug = require('debug')('app:middleware');

module.exports = (params = false) => {
  return (req, res, next) => { 
    
    // parse auth cookie
    try {
      req.cookies.auth = JSON.parse(req.cookies.auth);
    } catch (error) {
    }

    debug('VerifyAuth Middleware get called with params: '+ params);
    debug('cookies: ' + JSON.stringify(req.cookies));

    if(params === 'auth') {

      if(req.cookies.auth && req.cookies.auth.id) {
        return next()
      } else {
        debug('redirecting to /login...');
        return res.redirect('/login');
      }

    } else if(params === 'notAuth') {

      if(!req.cookies.auth || !req.cookies.auth.id) {
        return next();
      } else {
        debug('redirecting to /home...');
        return res.redirect('/home');
      }

    } else {
      throw "Verify Auth: params should be (notAuth or auth)";
    }

  }
}